<?php

include 'Controller/Controller.php';

$character_id = filter_input(INPUT_POST, 'character_id', FILTER_VALIDATE_INT);

$controller = new Controller();
$character = $controller->getCharacter($character_id);

?>
<div class="row">
  <div class="col-6">
    <label>Name</label>
  </div>
  <div class="col-6">
    <label><?=$character['name']?>
  </div>
</div>
<div class="row">
  <div class="col-6">
    <label>Description</label>
  </div>
  <div class="col-6">
    <label><?=$character['description']?>
  </div>
</div>
<div class="row">
  <div class="col-6">
    <label>Comics</label>
  </div>
  <div class="col-6">
    <ul>
      <?php
        foreach ($character['comics']['items'] as $comic) {
          echo "<li>" . $comic['name'] . "</li>";
        }
      ?>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-6">
    <label>Series</label>
  </div>
  <div class="col-6">
    <ul>
      <?php
        foreach ($character['series']['items'] as $serie) {
          echo "<li>" . $serie['name'] . "</li>";
        }
      ?>
    </ul>
  </div>
</div>
</div>