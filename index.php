<?php
  include 'Controller/Controller.php';

  $controller = new Controller();
  $comics = $controller->getCharacters();
?>
<html>
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.12.1/datatables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css"/>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <div class="text-center mb-5">
      <h2>Sitdigital test</h2>
    </div>
    <hr>
    <!--Main layout-->
    <main style="display: flex;">
        <header>
            <div id="mySidenav" class="sidenav">
                <a href="https://developer.marvel.com/" target="_blank">Marvel Developer</a>
                <a href="https://www.marvel.com/comics?&options%5Boffset%5D=0&totalcount=12" target="_blank">Marvel Comics</a>
                <a href="https://www.marvel.com/tv-shows" target="_blank">Marvel Tv shows</a>
                <a href="https://www.linkedin.com/in/luis-fernando-rodr%C3%ADguez-navarro-19164927/" target='_blank'>About me!</a>
            </div>
        </header>
        <div class="container pt-4">
            <div class="mb-5">
                <div class="mb-5">
                    <h3>Marvel Data</h3>
                </div>
                <table id="marvel_data" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Detail</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </main>
    <footer class="footer py-4">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-lg-between">
            <div class="col-lg-6 mb-lg-0 mb-4">
                <div class="copyright text-center text-sm text-muted text-lg-start">
                    © 2022 MARVEL
                </div>
            </div>
            <div class="col-lg-6">
                <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                    <li class="nav-item">
                        <a href="http://marvel.com\">Data provided by Marvel. © 2022 MARVEL</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
    <!--Main layout-->
  </body>
  <script>
    $(document).ready(function() {

      //start datatables
      $('#marvel_data').DataTable({
        "data": <?=json_encode($comics)?>,
        "columns" : [
            { "data" : "title" },
            { "data" : "image" },
            { "data" : "url"}
        ]
      });

      $('.character-detail').on('click', function() {
        url = $(this).data('url');
        character_id = $(this).data('character');
        $.ajax({
          url: url,
          method: 'post',
          data: {
            character_id: character_id
          },
          success:function(html) {
              $('.modal-body').html(html);
              $('#myModal').modal({show:true});
          }
        })
      });
    });
  </script>
  <div class="modal fade modal-xl" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Character details</h5>
        </div>
        <div class="modal-body"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</html>
