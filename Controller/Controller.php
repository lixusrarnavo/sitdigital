<?php

include 'API/CURL.php';

class Controller
{

    protected CURL $curl;

    public function __construct()
    {
        $this->curl = new CURL();
    }

    public function getCharacters(): array
    {
        $data = $this->curl->getCharacters();
        $new_data = [];

        foreach ($data as $dat) {
            $dat['thumbnail']['path'] = str_replace('http', 'https', $dat['thumbnail']['path']);
            $new_data[] = [
                'title' => $dat['name'],
                'image' => "<img src=" . $dat['thumbnail']['path'] . ">",
                'url' => "<button data-url='character_detail.php' data-character='" . $dat['id'] . "' class='btn btn-primary character-detail'>Character Detail</button>"
            ];
        }
        
        return $new_data;
    }

    public function getCharacter(int $character_id): array
    {
        $character_data = [];
        $characters = $this->curl->getCharacters();

        foreach ($characters as $character) {
            if ($character['id'] == $character_id) {
                $character_data = $character;
            }
        }

        return $character_data;
    }
}