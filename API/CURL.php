<?php

class CURL {

  protected $curl;
  protected $hash = '';
  protected $url = '';
  protected $ts = '';

  public function __construct()
  {
    $this->curl = curl_init();
    $this->ts = strtotime('now');
    $this->hash = md5($this->ts . '13d9d4ac411ecfc36d38253537353da9b311f2de73a203b139b7c20a9de8d9ba2d32b207');
    $this->url = "https://gateway.marvel.com/v1/public/characters?ts=" . $this->ts . "&apikey=73a203b139b7c20a9de8d9ba2d32b207&hash=" . $this->hash;

    curl_setopt_array($this->curl, array(
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_URL => $this->url,
      CURLOPT_CUSTOMREQUEST => "GET"
    ));
  }

  /**
  * Function that returns a list of orders, it can be filtered by dates
  **/
  public function getCharacters(): array {
      $data = $this->execute();
      return $data;
  }

  protected function execute(): array
  {
      $response = curl_exec($this->curl);
      $err = curl_error($this->curl);

      curl_close($this->curl);

      if ($err) {
    	   throw new Exception($err);
       } else {
    	    $data = json_decode($response, true);
          return $data['data']['results'];
        }
  }
}
